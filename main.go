package main

import (
	"auth-session/pkg/authenticate"
	"auth-session/pkg/middleware"
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-contrib/sessions/cookie"
	"github.com/gin-gonic/gin"
)

const (
	COOKIE_SECRET = "secret"
)

func main() {
	r := gin.Default()

	r.StaticFS("/assets", http.Dir("./public/assets"))
	r.LoadHTMLGlob("./public/template/*")

	store := cookie.NewStore([]byte(COOKIE_SECRET))

	r.Use(sessions.Sessions("snip-session", store))

	r.GET("/", indexHTML)

	// public
	r.GET("/livez", checkHealth)
	r.POST("/login", authenticate.Login)
	r.GET("/logout", authenticate.Logout)

	// private
	private := r.Group("/private")
	private.Use(middleware.Authentication)
	private.GET("message", getMessage)

	r.Run(":3000")
}

func indexHTML(ctx *gin.Context) {
	session := sessions.Default(ctx)
	sessionID := session.Get("id")
	if sessionID == nil {
		ctx.HTML(http.StatusOK, "index.html", nil) // need to LoadHTMLGlob
		return
	}
	ctx.String(http.StatusOK, "you have been login.")
}

// func indexHTML(ctx *gin.Context) {
// 	f, _ := os.Open("public/index.html")
// 	b, _ := io.ReadAll(f)
// 	ctx.Data(http.StatusOK, "text/html", b)
// }

func checkHealth(ctx *gin.Context) {
	ctx.String(http.StatusOK, "OK")
}

func getMessage(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, gin.H{
		"message": "this is private message",
	})
}
