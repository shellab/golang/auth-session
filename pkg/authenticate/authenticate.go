package authenticate

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func Login(ctx *gin.Context) {
	user := ctx.PostForm("user")
	pass := ctx.PostForm("pass")

	session := sessions.Default(ctx)

	if user == "dachi" && pass == "1234" {
		session.Set("id", "dachi")
		session.Save()
		ctx.JSON(http.StatusOK, gin.H{
			"message": "login success.",
		})
		return
	}

	ctx.JSON(http.StatusBadRequest, gin.H{
		"message": "login failed.",
	})
}

func Logout(ctx *gin.Context) {
	session := sessions.Default(ctx)
	session.Clear()
	session.Save()

	ctx.JSON(http.StatusBadRequest, gin.H{
		"message": "logout.",
	})
}
