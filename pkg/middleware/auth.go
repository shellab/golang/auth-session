package middleware

import (
	"net/http"

	"github.com/gin-contrib/sessions"
	"github.com/gin-gonic/gin"
)

func Authentication(ctx *gin.Context) {
	session := sessions.Default(ctx)
	sessionID := session.Get("id")
	if sessionID == nil {
		ctx.AbortWithStatusJSON(http.StatusNotFound, gin.H{
			"message": "unauthorized",
		})
	}
	ctx.Next()
}
